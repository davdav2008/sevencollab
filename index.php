<?php
require(__DIR__ . '/src/Autoloader.php');
Autoloader::register();
$uri = $_SERVER['REQUEST_URI'];
$request_method = $_SERVER['REQUEST_METHOD'];
$user = new User();
switch ($uri){
    case '/get':
            return $user->getAction();
            break;
    case '/post':
        if ($request_method=='POST') {
            return $user->postAction();
            break;
        }
    default:
        return $user->notFoundAction();
        break;
}


