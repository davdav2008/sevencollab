<?php

 class Viewer
{

     private $list;
     private $form;
     /**
      * Viewer constructor.
      *
      * @param $view_type string|array
      * @param $data array
      */
     public function __construct($view_type, $data=[])
     {
         if (is_array($view_type)){
             foreach ($view_type as $type){
                 $view = $this->getViewByType($type);
                 if ($this->needToPopulate($type)){
                     $view = $this->populateViewWithData($view, $data);
                 }
                 $this->assignBlock($view, $type);
             }
         }else{
             $view = $this->getViewByType($view_type);
             if ($this->needToPopulate($view)){
                 $view = $this->populateViewWithData($view, $data);
             }
             $this->assignBlock($view, $view_type);
         }
     }


     /**
      * @param $view_type string
      * @return string
      */
     private function getViewByType($view_type)
     {
         switch ($view_type){
             case 'form':
                 $view = file_get_contents('./views/form.html');
                 break;
             case 'list':
                 $view = file_get_contents('./views/list.html');
                 break;
             case 'page':
                 $view = file_get_contents('./views/fullPage.html');
                 break;
         }

         return $view;
     }

     /**
      * @param $view_type string
      * @return bool
      */
     private function needToPopulate($view_type)
     {
         switch ($view_type){
             case 'form':
             case 'list':
                 return true;
                 break;
             default:
                 return false;
                 break;
         }
     }


     /**
      * @param $view string
      * @param $data array
      * @return string
      */
     private function populateViewWithData($view, $data)
     {
         foreach ($data as $slug=>$value){
             $view = str_replace('{' . $slug . '}', $value, $view);
         }

         return $view;
     }

     private function assignBlock($view, $view_type)
     {
         switch ($view_type){
             case 'form':
                 $this->form = $view;
                 break;
             case 'list':
                 $this->list = $view;
                 break;
         }

     }

     /**
      * @param $view string
      * @return mixed|string
      */
     public function builtPage(){
         $view = $this->getViewByType('page');
         $view = str_replace('{form}', $this->form, $view);
         $view = str_replace('{list}', $this->list, $view);
         echo $view;
     }

 }
