<?php

class User extends Controller
{
    const JSON_PATH = "./formResults.json";
    public function getAction()
    {
        $jsonDataArray = json_decode($this->getJsonContent(), true);
        $views_list = ['form', 'list'];
        $views_data = ['form.action'=>'http://'.$_SERVER['HTTP_HOST'].'/post', 'list.list'=>$this->makeListFromArray($jsonDataArray)];
        $viewer = new Viewer($views_list, $views_data);
        $viewer->builtPage();
    }
    public function postAction()
    {
        $formData = array(
            'name'=> $_POST['name'],
            'email'=> $_POST['email']
        );
        $json = $this->getUpdatedJsonContent($formData);
        $this->updateJson($json);
        $this->getPage();
    }

    private function getJsonContent()
    {
        return file_get_contents(self::JSON_PATH);
    }

    private function makeListFromArray($array=[]){
        $table='<table>';
        $table.='<tr>';
        $table.='<td>NAME</td>';
        $table.='<td>EMAIL</td>';
        $table.='</td>';
        if (!empty($array)){
            foreach ($array as $row){
                $table.='<tr><td>';
                $table.=$row['name'];
                $table.='</td>';
                $table.='<td>';
                $table.=$row['email'];
                $table.='</td>';
                $table.='</tr>';
            }
        }
        $table.='</table>';
        return $table;
    }

    private function getUpdatedJsonContent($newData)
    {   $json = $this->getJsonContent();
        if (!$this->checkIfJsonFileNotEmpty($json)){
            $jsonDataArray = [];
        }else{
            $jsonDataArray = json_decode($json, true);
        }
        array_push($jsonDataArray, $newData);
        $json = json_encode($jsonDataArray, JSON_PRETTY_PRINT);
        return $json;
    }

    private function updateJson($json)
    {
        file_put_contents(self::JSON_PATH, $json);
    }

    private function checkIfJsonFileNotEmpty($json)
    {
        if ($json!==''&&!is_null($json)){
            return true;
        }
        return false;
    }

    public function getPage(){
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/get');
        exit;
    }

    public function notFoundAction()
    {
        parent::notFoundAction();
    }


}
