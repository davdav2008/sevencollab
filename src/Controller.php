<?php

abstract class Controller
{
    abstract function getAction();
    abstract function postAction();
    public function notFoundAction(){
        include('../sevencollab/views/404.php');
    }
}
